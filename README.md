<img src="./assets/lp-logo.png" alt="Ligma Prods" height="60"/>
<img src="https://symbology.dev/assets/hero.svg" alt="Symbology" height="60"/>

# QR Generator (Symbology NodeJS)
https://symbology.dev/


## Requisitos
- node 14.x o superior
- npm


## Instrucciones:

### 1- Instalar dependencias
 ```
 npm install
 ```
 
### 2- Definir .env
  - FILENAME = "*nombre*.txt"
  - OUTPUT_FILENAME = "*nombre*.html"

### 3- Copiar archivo en carpeta de proyecto con el nombre de FILENAME
    
### 2- Ejecutar
 ```
 npm run start
 ```
