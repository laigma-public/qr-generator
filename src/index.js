import dotenv from 'dotenv'
import prompt from 'prompt'
import {
  SymbologyType,
  OutputType,
  EncodingMode,
  createStream
} from 'symbology'
import fs from 'fs'
import open from "open";
import pdf from "html-pdf-node";

dotenv.config();
prompt.start();

console.log('Archivo de entrada: ', process.env.FILENAME);
console.log('Archivo de salida: ', process.env.OUTPUT_FILENAME);

async function main() {
  const inputData = fs.readFileSync(`./${process.env.FILENAME}`, 'utf8')
  const itemsList = inputData.split('\n');

  // Solicitamos ancho de QR
  const { width } = await prompt.get([{
    name: 'width',
    description: 'Introduce el ancho/alto de salida en pixeles (la imagen será cuadrada)',
    type: 'number',
    required: true
  }]);

  // Parseamos lista de entrada
  let rows = [];
  for (const item of itemsList) {
    if (item) {
      const qrData = await createCode(item);
      rows.push({ data: qrData, ref: item });
    }
  }

  // Convertimos en html
  let htmlBody = `<!DOCTYPE html><html>`;
  let pageHeight = 0;
  rows.forEach(row => {
    const fontSize = parseInt(width) > 130 ? 40 : 20;
    htmlBody +=
      `<div 
        style="
          background-color: #F0FF0F;
          margin: 0px; 
          padding-top: ${width / 2}px;
          padding-bottom: ${width / 2}px;          
          border-bottom: solid black 4px;
          text-align: center; 
          align-items: center; 
          display: flex;
          justify-content: space-evenly;
        ">
          <span style="font-size: ${fontSize}px; font-weight: bold;">${row.ref}</span>
          <div style="height: ${width}px; width: ${width}px; overflow: hidden;">
            <img style="display: block; margin: 0px; padding: 0px;" src="${row.data}" width="${width}"/>
          </div>
        </div>`;
    pageHeight += width * 2;
    if (pageHeight > 820) {
      htmlBody += `<div style="page-break-before: always;"></div>`;
      pageHeight = 0;
    }
  });
  htmlBody += `</html>`;
  // fs.writeFileSync(`./${process.env.OUTPUT_FILENAME}`, htmlBody);
  // await open(`./${process.env.OUTPUT_FILENAME}`, { "wait": true });

  // Transformamos en pdf
  const res = await createPdf(htmlBody);
  if (res === "OK") {
    await open(`./${process.env.PDF_OUTPUT}`, { "wait": true });
  }
}


// Funciones auxiliares

async function createCode(data) {
  const result = await createStream({
    symbology: SymbologyType.QRCODE,
    encoding: EncodingMode.UNICODE_MODE,
  }, data, OutputType.PNG);
  const splitData = result.data.split(',');
  return `${splitData[0]}, ${splitData[1]}`;
};

function createPdf(htmlBody) {
  const document = { content: htmlBody };
  const options = { format: "A4" };
  return pdf.generatePdf(document, options)
    .then((res) => {
      fs.writeFileSync(`./${process.env.PDF_OUTPUT}`, res);
      return "OK";
    })
    .catch((error) => {
      console.error(error);
    });
};



main();